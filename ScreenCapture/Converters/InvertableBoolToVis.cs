﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace ScreenCapture.Converters
{
	[ValueConversion(typeof(bool), typeof(Visibility))]
	public class InvertableBoolToVis : IValueConverter
	{

		public object Convert(object value, Type targetType,
							  object parameter, CultureInfo culture)
		{
			var boolValue = (bool)value;

			if (parameter != null)
			{
				var type = (string)parameter;

				if (string.Equals(type, "inverse", StringComparison.OrdinalIgnoreCase))
					return !boolValue ? Visibility.Visible : Visibility.Collapsed;
			}

			return boolValue ? Visibility.Visible : Visibility.Collapsed;
		}

		public object ConvertBack(object value, Type targetType,
			object parameter, CultureInfo culture)
		{
			return null;
		}
	}
}
