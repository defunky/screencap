﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Shapes;

namespace ScreenCapture.Themes
{
	public class Window : System.Windows.Window
	{
		private HwndSource m_hwndSource;

		static Window()
		{
			DefaultStyleKeyProperty.OverrideMetadata(typeof(Window), new FrameworkPropertyMetadata(typeof(Window)));
		}

		public Window()
		{
			PreviewMouseMove += Window_PreviewMouseMove;
		}

		private void Window_PreviewMouseMove(object sender, MouseEventArgs e)
		{
			if (Mouse.LeftButton != MouseButtonState.Pressed)
				Cursor = Cursors.Arrow;
		}

		public override void OnApplyTemplate()
		{
			var titleBar = GetTemplateChild("TitleBar") as Grid;
			if (titleBar != null)
				titleBar.MouseDown += TitleBar_MouseDown;

			var minBtn = GetTemplateChild("MinimizeButton") as Button;
			if(minBtn != null)
				minBtn.Click += MinBtn_Click;

			var maxBtn = GetTemplateChild("MaximizeButton") as Button;
			if (maxBtn != null)
				maxBtn.Click += MaxBtn_Click;

			var closeBtn = GetTemplateChild("CloseButton") as Button;
			if (closeBtn != null)
				closeBtn.Click += CloseBtn_Click;

			Grid resizeGrid = GetTemplateChild("resizeGrid") as Grid;
			if (resizeGrid != null)
			{
				foreach (UIElement element in resizeGrid.Children)
				{
					Rectangle resizeRectangle = element as Rectangle;
					if (resizeRectangle != null)
					{
						resizeRectangle.PreviewMouseDown += ResizeRectangle_PreviewMouseDown;
						resizeRectangle.MouseMove += ResizeRectangle_MouseMove;
					}
				}
			}

			base.OnApplyTemplate();
		}

		

		private void TitleBar_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			if (e.ClickCount == 2)
				MaxBtn_Click(sender, e);
			else
				DragMove();
		}

		private void CloseBtn_Click(object sender, RoutedEventArgs e)
		{
			Close();
		}

		private void MaxBtn_Click(object sender, RoutedEventArgs e)
		{
			WindowState = WindowState == WindowState.Maximized ? WindowState.Normal : WindowState.Maximized;
		}

		private void MinBtn_Click(object sender, RoutedEventArgs e)
		{
			WindowState = WindowState.Minimized;
		}

		#region resize logic
		//We use Win32 api to resize stuff
		protected override void OnInitialized(EventArgs e)
		{
			SourceInitialized += OnSourceInitialized;
			base.OnInitialized(e);
		}

		private void OnSourceInitialized(object sender, EventArgs e)
		{
			m_hwndSource = (HwndSource)PresentationSource.FromVisual(this);
		}
		private void ResizeRectangle_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
		{
			Rectangle rectangle = sender as Rectangle;
			if (rectangle == null) return;

			switch (rectangle.Name)
			{
				case "top":
					Cursor = Cursors.SizeNS;
					break;
				case "bottom":
					Cursor = Cursors.SizeNS;
					break;
				case "left":
					Cursor = Cursors.SizeWE;
					break;
				case "right":
					Cursor = Cursors.SizeWE;
					break;
				case "topLeft":
					Cursor = Cursors.SizeNWSE;
					break;
				case "topRight":
					Cursor = Cursors.SizeNESW;
					break;
				case "bottomLeft":
					Cursor = Cursors.SizeNESW;
					break;
				case "bottomRight":
					Cursor = Cursors.SizeNWSE;
					break;
			}
		}


		[DllImport("user32.dll", CharSet = CharSet.Auto)]
		private static extern IntPtr SendMessage(IntPtr hWnd, UInt32 msg, IntPtr wParam, IntPtr lParam);

		protected void ResizeRectangle_PreviewMouseDown(object sender, MouseButtonEventArgs e)
		{
			Rectangle rectangle = sender as Rectangle;

			if (rectangle == null) return;
			switch (rectangle.Name)
			{
				case "top":
					Cursor = Cursors.SizeNS;
					ResizeWindow(ResizeDirection.Top);
					break;
				case "bottom":
					Cursor = Cursors.SizeNS;
					ResizeWindow(ResizeDirection.Bottom);
					break;
				case "left":
					Cursor = Cursors.SizeWE;
					ResizeWindow(ResizeDirection.Left);
					break;
				case "right":
					Cursor = Cursors.SizeWE;
					ResizeWindow(ResizeDirection.Right);
					break;
				case "topLeft":
					Cursor = Cursors.SizeNWSE;
					ResizeWindow(ResizeDirection.TopLeft);
					break;
				case "topRight":
					Cursor = Cursors.SizeNESW;
					ResizeWindow(ResizeDirection.TopRight);
					break;
				case "bottomLeft":
					Cursor = Cursors.SizeNESW;
					ResizeWindow(ResizeDirection.BottomLeft);
					break;
				case "bottomRight":
					Cursor = Cursors.SizeNWSE;
					ResizeWindow(ResizeDirection.BottomRight);
					break;
			}
		}

		private void ResizeWindow(ResizeDirection direction)
		{
			SendMessage(m_hwndSource.Handle, 0x112, (IntPtr)(61440 + direction), IntPtr.Zero);
		}

		private enum ResizeDirection
		{
			Left = 1,
			Right = 2,
			Top = 3,
			TopLeft = 4,
			TopRight = 5,
			Bottom = 6,
			BottomLeft = 7,
			BottomRight = 8,
		}
#endregion
	}
}
