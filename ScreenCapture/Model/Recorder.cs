﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows;
using GalaSoft.MvvmLight.Threading;
using ScreenCapture.Helpers;
using Path = System.IO.Path;
using Point = System.Windows.Point;
using Rectangle = System.Windows.Shapes.Rectangle;

namespace ScreenCapture.Model
{
	public class Recorder
	{
		private readonly Rectangle m_region;
		private List<Bitmap> m_recordedList;
		private MultimediaTimer m_timer;
		private int m_fps = 30;
		private string m_tempPath;

		public Recorder()
		{
			//Find the screen capture region
			m_region = UIHelpers.FindVisualChildByName<Rectangle>(Application.Current.MainWindow, "CaptureImage");
			m_timer = new MultimediaTimer(1000 / FPS);
			m_recordedList = new List<Bitmap>();
			createTempDir();
			m_timer.Tick += (sender, args) => screenCapture();
		}

		~Recorder()
		{
			Directory.Delete(m_tempPath, true);
		}

		[SuppressMessage("ReSharper", "AssignNullToNotNullAttribute")]
		private void createTempDir()
		{
			//Location of the executable
			m_tempPath = Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), "TEMP");

			if (!Directory.Exists(m_tempPath))
			{
				DirectoryInfo dir = Directory.CreateDirectory(m_tempPath);
				dir.Attributes = FileAttributes.Directory | FileAttributes.Hidden;
			}
		}

		public void Start()
		{
			m_timer.Start();
		}

		public void Stop()
		{
			m_timer.Stop();
			for (int i = 0; i < m_recordedList.Count; i++) {
				m_recordedList[i].Save(Path.Combine(m_tempPath, "Frame " + i + ".png"), ImageFormat.Png);
				m_recordedList[i].Dispose();
			}
			m_recordedList.Clear();
		}

		private void screenCapture()
		{
			Point position = new Point();

			//Run on dispatcher can't access UI on non-UI thread.
			DispatcherHelper.UIDispatcher.Invoke(() => position = m_region.PointToScreen(new Point(0d, 0d)));
			Bitmap bmpScreenCapture = new Bitmap((int)m_region.ActualWidth, (int)m_region.ActualHeight);

			using (Graphics g = Graphics.FromImage(bmpScreenCapture)) {

				g.CopyFromScreen((int)position.X,
								 (int)position.Y,
								 0, 0,
								 bmpScreenCapture.Size,
								 CopyPixelOperation.SourceCopy);

				m_recordedList.Add(bmpScreenCapture);
			}
		}

		public int FPS
		{
			get { return m_fps; }
			set
			{
				m_fps = value;
				if (m_timer != null)
					m_timer.Interval = 1000 / m_fps;
			}
		}
	}
}
