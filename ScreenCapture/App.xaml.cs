﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using GalaSoft.MvvmLight.Threading;

namespace ScreenCapture
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
		[STAThread]
		public static void Main()
		{
			var application = new App();
			application.InitializeComponent();
			DispatcherHelper.Initialize();
			application.Run();
		}

		public static string Version => System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
	}
}
