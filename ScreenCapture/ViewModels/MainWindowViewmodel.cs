﻿using System;
using System.Diagnostics;
using System.Threading;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using ScreenCapture.Helpers;
using ScreenCapture.Model;

namespace ScreenCapture.Viewmodels
{
	public class MainWindowViewModel : ViewModelBase
	{

		private bool m_isRecording;
		private MultimediaTimer m_timer;
		private Recorder m_recorder;


		public MainWindowViewModel()
		{
			m_recorder = new Recorder();

			Record = new RelayCommand(startRecording);
			Stop = new RelayCommand(stopRecording);
		}

		private void startRecording()
		{
			m_recorder.Start();
			IsRecording = true;
		}


		private void stopRecording()
		{
			m_recorder.Stop();
			IsRecording = false;
		}

		#region properties

		public RelayCommand Record { get; internal set; }
		public RelayCommand Stop { get; internal set; }

		public bool IsRecording
		{
			get { return m_isRecording; }
			set { Set(ref m_isRecording, value); }
		}

		#endregion


	}
}
