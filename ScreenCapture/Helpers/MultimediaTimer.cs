﻿using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Windows.Controls.Primitives;

namespace ScreenCapture.Helpers
{

	/// <summary>
	/// Timer types for MultiMedia timer
	/// 
	/// Single = Timer event occurs once
	/// Peroidic = Timer event happens peroidically
	/// </summary>
	public enum TimerType
	{
		Single = 0,
		Peroidic = 1
	}

	/// <summary>
	/// A wrapper for Win32 High resolution Multimedia Timer
	/// </summary>
	public class MultimediaTimer : IDisposable
	{

		private int m_resolution, m_interval;
		private volatile uint m_timerId;

		private readonly TimerCallback m_unmanagedTimerCallback;
		public event EventHandler Tick;


		private bool m_disposed = false;


		/// <summary>
		/// 
		/// </summary>
		/// <param name="interval">The time interval in ms</param>
		/// <param name="resolution">The resolution of timer (lower is more accurate but more intensive)</param>
		/// <param name="mode">The Timertype either one-time use or perodically</param>
		public MultimediaTimer(int interval, int resolution = 0, TimerType mode = TimerType.Peroidic)
		{
			m_interval = interval; //in ms
			m_resolution = resolution;
			Mode = mode;
			m_unmanagedTimerCallback = ManagedTimerCallback;
		}

		public void Dispose()
		{
			if (m_disposed)
				return;

			if (IsRunning)
				Stop();

			m_disposed = true;
		}


		public void Start()
		{
			if (m_disposed)
				throw new ObjectDisposedException("This timer instance has been disposed.");

			m_timerId = TimeSetEvent((uint)Interval, (uint)Resolution, m_unmanagedTimerCallback, 0, (int)Mode);

			if (m_timerId == 0) {
				int error = Marshal.GetLastWin32Error();
				throw new Win32Exception(error);
			}
		}

		public void Stop()
		{
			TimeKillEvent(m_timerId);
			m_timerId = 0;
		}

		private void ManagedTimerCallback(uint timerid, uint msg, IntPtr user, uint dw1, uint dw2)
		{
			var handler = Tick;
			handler?.Invoke(this, EventArgs.Empty);
		}

		public int Resolution
		{
			get { return m_resolution; }
			set
			{
				if (value < 0)
					throw new ArgumentOutOfRangeException("Resolution cannot be negative: " + value);

				m_resolution = value;
			}
		}

		public int Interval
		{
			get { return m_interval; }
			set
			{
				if (value < 0)
					throw new ArgumentOutOfRangeException("Interval cannot be negative: " + value);

				m_interval = value;

				if (Resolution > Interval)
					Resolution = value;
			}
		}

		public TimerType Mode { get; set; }

		public bool IsRunning => m_timerId != 0;

		#region Dllimport

		[DllImport("winmm.dll", SetLastError = true, EntryPoint = "timeSetEvent")]
		private static extern uint TimeSetEvent(
			uint uDelay,
			uint uResolution,
			TimerCallback callback,
			uint dwUser,
			int fuEvent
			);

		[DllImport("winmm.dll", SetLastError = true, EntryPoint = "timeKillEvent")]
		private static extern uint TimeKillEvent(uint uTimerID);

		private delegate void TimerCallback(uint timerid, uint msg, IntPtr user, uint dw1, uint dw2);

		#endregion

	}
}
